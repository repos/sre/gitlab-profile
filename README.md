When migrating or creating a new repository please follow these naming conventions:

- Use spaces or hyphens as delimiters in the human readable repository names to ensure predictable repository URLs (spaces are replaced with hyphens, other characters such as underscores are not)
- If the repository is a part of a larger project, choose one of:
  - Create a group (e.g. miscweb/bugzilla, miscweb/annualreport)
  - Use a prefix (e.g. miscweb-bugzilla, miscweb-annualreport)
- Only create groups for projects (e.g. miscweb), avoid creating them for functional purposes (e.g. debs or software) as these designations can change over time